<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $register_date;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\LessThan("today")
     */
    private $date_of_birth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Role", inversedBy="role")
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CourseUser", mappedBy="user")
     */
    private $userCourse;

    public function __construct()
    {
        $this->userCourse = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->register_date;
    }

    public function setRegisterDate(?\DateTimeInterface $register_date): self
    {
        $this->register_date = $register_date;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->date_of_birth;
    }

    public function setDateOfBirth(?\DateTimeInterface $date_of_birth): self
    {
        $this->date_of_birth = $date_of_birth;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function __toString()
    {
        return $this->last_name .' '.$this->first_name;;
    }

    /**
     * @return Collection|CourseUser[]
     */
    public function getCourseUser(): Collection
    {
        return $this->courseUser;
    }

    public function addCourseUser(CourseUser $courseUser): self
    {
        if (!$this->courseUser->contains($courseUser)) {
            $this->courseUser[] = $courseUser;
            $courseUser->setUser($this);
        }

        return $this;
    }

    public function removeCourseUser(CourseUser $courseUser): self
    {
        if ($this->courseUser->contains($courseUser)) {
            $this->courseUser->removeElement($courseUser);
            // set the owning side to null (unless already changed)
            if ($courseUser->getUser() === $this) {
                $courseUser->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CourseUser[]
     */
    public function getUserCourse(): Collection
    {
        return $this->userCourse;
    }

    public function addUserCourse(CourseUser $userCourse): self
    {
        if (!$this->userCourse->contains($userCourse)) {
            $this->userCourse[] = $userCourse;
            $userCourse->setUser($this);
        }

        return $this;
    }

    public function removeUserCourse(CourseUser $userCourse): self
    {
        if ($this->userCourse->contains($userCourse)) {
            $this->userCourse->removeElement($userCourse);
            // set the owning side to null (unless already changed)
            if ($userCourse->getUser() === $this) {
                $userCourse->setUser(null);
            }
        }

        return $this;
    }
}
