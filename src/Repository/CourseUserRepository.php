<?php

namespace App\Repository;

use App\Entity\CourseUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method CourseUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseUser[]    findAll()
 * @method CourseUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseUser::class);
    }

    // /**
    //  * @return CourseUser[] Returns an array of CourseUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseUser
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAverageGradeOnCourse($courseId)
    {
        $queryAverageGradeOnCourse = $this->createQueryBuilder('c')
            ->select("AVG(c.points)")
            ->where("c.course = $courseId")
            ->getQuery();

        $result = $queryAverageGradeOnCourse->getResult();

        return $result[0];
    }

    public function getGrades($courseId)
    {
        $queryGetGrades = $this->createQueryBuilder('c')
            ->select('c.points, count(c.points) as count')
            ->where("c.course = $courseId")
            ->groupBy('c.points')
            ->getQuery();

        $result = $queryGetGrades->getScalarResult();

        return $result;
    }
}
