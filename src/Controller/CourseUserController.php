<?php

namespace App\Controller;

use App\Entity\CourseUser;
use App\Form\CourseUserType;
use App\Repository\CourseUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/course_user")
 */
class CourseUserController extends AbstractController
{
    /**
     * @Route("/", name="course_user_index", methods={"GET"})
     */
    public function index(CourseUserRepository $courseUserRepository): Response
    {
        return $this->render('course_user/index.html.twig', [
            'course_users' => $courseUserRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="course_user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $courseUser = new CourseUser();
        $form = $this->createForm(CourseUserType::class, $courseUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($courseUser);
            $entityManager->flush();

            return $this->redirectToRoute('course_user_index');
        }

        return $this->render('course_user/new.html.twig', [
            'course_user' => $courseUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="course_user_show", methods={"GET"})
     */
    public function show(CourseUser $courseUser): Response
    {
        return $this->render('course_user/show.html.twig', [
            'course_user' => $courseUser,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="course_user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CourseUser $courseUser): Response
    {
        $form = $this->createForm(CourseUserType::class, $courseUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('course_user_index');
        }

        return $this->render('course_user/edit.html.twig', [
            'course_user' => $courseUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="course_user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CourseUser $courseUser): Response
    {
        if ($this->isCsrfTokenValid('delete'.$courseUser->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($courseUser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('course_user_index');
    }
}
